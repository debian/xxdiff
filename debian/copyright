Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: xxdiff
Source: https://github.com/blais/xxdiff
Files-Excluded: doc/screenshots
Comment: The upstream hg source contains a screenshots gallery intended
 for the author's web site, based on a non-free JavaScript library. The
 doc/screenshots directory was removed to create the +dfsg source.

Files: *
Copyright: 1999-2006, Martin Blais <blais@furius.ca>
License: GPL-2+

Files: src/getopt* adm/config.guess
Copyright: 1987-2000, Free Software Foundation, Inc.
License: GPL-2+

Files: old/autoconf/acinclude.m4
Copyright: 1997, Janos Farkas (chexum@shadow.banki.hu)
 1997-1999, Stephan Kulow (coolo@kde.org)
 1994-1998, Free Software Foundation, Inc.
License: LGPL-2+

Files: old/autoconf/automoc
Copyright: 1998, Kalle Dalheimer <kalle@kde.org>
License: automoc
 This is really free software, unencumbered by the GPL.
 You can do anything you like with it except sueing me.

Files: adm/xxdiff-mdk.spec tools/tkxxdiff/tkxxdiff
Copyright: 2007, Philippe Corbes <philippe.corbes@laposte.net>
License: GPL-2+

Files: debian/*
Copyright: 2001-2007, Tomas Pospisek <tpo_deb@sourcepole.ch>
 2007-2008, Y Giridhar Appaji Nag <giridhar@appaji.net>
 2014-2024, Florian Schlichting <fsfs@debian.org>
License: GPL-2+

License: GPL-2+
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 This package is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this program. If not, see <http://www.gnu.org/licenses/>.
 .
 On Debian systems, the complete text of the GNU General
 Public License version 2 can be found in "/usr/share/common-licenses/GPL-2".

License: LGPL-2+
 This program is free software: you can redistribute it and/or modify it
 under the terms of the GNU Library General Public License as published
 by the Free Software Foundation, either version 2 of the License, or
 (at your option) any later version.
 .
 On Debian systems, the complete text of the GNU Library General
 Public License version 2 can be found in "/usr/share/common-licenses/LGPL-2".
